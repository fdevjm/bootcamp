// Reducers provide the value of state (not the key of state)
export default function() {
  return [
    { title: 'Javascript: The Good Parts', pages: 101 },
    { title: 'Harry Potter', pages: 39 },
    { title: 'The Dark Tower', pages: 10 },
    { title: 'Eloquent Ruby', pages: 2 }
  ]
}