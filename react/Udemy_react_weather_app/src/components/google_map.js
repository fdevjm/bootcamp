import React, { Component } from 'react';

/* eslint-disable */
class GoogleMap extends Component {

  cpomponentDidMount() {
    new google.maps.Map(this.refs.map, {
      zoom: 12,
      center: {
        lat: this.props.lat,
        lng: this.props.lon
      }
    })
  }
  
  render() {
    // this.refs.map
    // ref system in React. reference to HTML element that has been rendered to page
    return <div ref="map" />;
  }
}

export default GoogleMap;