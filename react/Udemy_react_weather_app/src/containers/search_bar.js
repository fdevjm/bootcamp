import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/';

class SearchBar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      term: ''
    }
    // this.onInputChange = this.onInputChange.bind(this);
  }

  render() {
    return (
      // prevent defaut action (button submit) form
      <form onSubmit={this.onFormSubmit} className="input-group">
        <input
        className="form-control"
        placeholder="Get a five-day forecase in your favorite cities."
        value={this.state.term}
        onChange={this.onInputChange}
        />
        <span className="input-group-btn">
          <button type="submit" className="btn btn-secondary">Submit</button>
        </span>
      </form>
    );
  }

  // Event handlers 'this' is some mysterious context.
  // Use Fat Arrow fuction, or you can use .bind (refer constructor) with default function syntax
  onInputChange = (event) => {
    this.setState({ term: event.target.value })
  }

  onFormSubmit = (event) => {
    this.setState({ term: '' });
    event.preventDefault();
    // We can fetch weather data here
    this.props.fetchWeather(this.state.term);
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchWeather }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);