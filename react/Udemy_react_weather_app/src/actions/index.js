import axios from 'axios';

const API_KEY = '574e2f04c4a68f8c2f0ec894cf843a07';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

/* eslint-disable */
export function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city},us`
  const request = axios.get(url); // This will return Promise

  console.log('Request: ' + request);

  // return always has to have 'type'
  return {
    type: FETCH_WEATHER,
    payload: request
  };
}