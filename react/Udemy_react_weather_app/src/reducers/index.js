import { combineReducers } from 'redux';
import WeatherReducer from './reducer_weather';

/* eslint-disable */
const rootReducer = combineReducers({
  weather: WeatherReducer,
});

export default rootReducer;
