import { FETCH_WEATHER } from '../actions/';

/* eslint-disable */
export default function(state = [], action) { // reducer is just a function
  console.log('Action received', action);
  switch(action.type) {
    case FETCH_WEATHER:
      return [ action.payload.data, ...state ];
  }
  return state;
}